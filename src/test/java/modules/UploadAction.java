package modules;

import helpers.Log;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import pageobjects.FileUploadHomePage;


public class UploadAction {

	public static void Execute(WebDriver driver, String file_path) throws Exception{
		
		FileUploadHomePage.upload.click();
		Log.info("Click action is performed on upload button in first page" );
		FileUploadHomePage.browse.sendKeys(file_path);		
		FileUploadHomePage.upload.click();
		Log.info("Click action is performed on upload button in second page" );
		Reporter.log("Upload button click Action is successfully performed");		
		//driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
}
