package step_definitions;

import static org.testng.AssertJUnit.assertEquals;

import helpers.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import modules.UploadAction;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

import pageobjects.FileUploadHomePage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class FileUploadStepDefinition{
    public WebDriver driver;
    
    public FileUploadStepDefinition()
    {
    	driver = Hooks.driver;
    }
    final String file_path = System.getenv("UPLOAD_FILE_PATH");
    String file_name =  FilenameUtils.getName(file_path);
    
    @Given("^I am on rean file upload website$")
    public void Iam_on_rean_fileupload_website() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.get("http://pipeline2-td-05142.testdrive.reancloud.com/fileupload/");
    }

    @Then("^I validate title and URL$")
    public void i_Validate_title_and_URL() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
      assertEquals("File Upload",driver.getTitle());
      assertEquals("http://pipeline2-td-05142.testdrive.reancloud.com/fileupload/",driver.getCurrentUrl());
    }
    
    @When("^I click Enter to upload button$")
    public void click_upload_button() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    	PageFactory.initElements(driver, FileUploadHomePage.class);
    	UploadAction.Execute(driver,file_path);
    }
    @And("^I should verify the new file that got updated$")
    public void verify_uploaded_file() throws Throwable {
    	
    	ArrayList<String> ar = new ArrayList<String>();
        // Write code here that turns the phrase above into concrete actions
    	PageFactory.initElements(driver, FileUploadHomePage.class);
    	List<WebElement> rows_table = FileUploadHomePage.s3table.findElements(By.tagName("tr"));
    	  //To calculate no of rows In table.
    	  int rows_count = rows_table.size();
    	  System.out.println(rows_count);
    	  //Loop will execute till the last row of table.
    	  for (int row=0; row<rows_count; row++){
    	   //To locate columns(cells) of that specific row.
    	   List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
    	   //To calculate no of columns(cells) In that specific row.
    	   int columns_count = Columns_row.size();    	   
    	   //Loop will execute till the last cell of that specific row.
    	   for (int column=0; column<columns_count-1; column++){
    	    //To retrieve text from that specific cell.
    	    String celtext = Columns_row.get(column).getText();
    	    ar.add(celtext);
    	    System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
    	   }    	   
    	  } 
    	 
    	  if (Arrays.asList(ar).toString().contains(file_name)){
    		  assertEquals("Check array has given file name", true,true );}
    	  else{
    		assertEquals("Check array has given file name", true,false );
    	  }    	      	 
    	  Log.info("Verified that given file exixts in table");

    	  Reporter.log("Verified that given file exixts in table");
    }
    
}