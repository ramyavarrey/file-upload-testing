package pageobjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
public class FileUploadHomePage extends BaseClass{

	public FileUploadHomePage(WebDriver driver){
		super(driver);
	}    

	
	@FindBy(how=How.CLASS_NAME, using="mybutton")
	public static WebElement upload;
	
	@FindBy(how=How.CLASS_NAME, using="table")
	public static WebElement s3table;
	
	//@FindBy(how=How.XPATH, using=".//*[@id='form2']/div/table")
	//public static WebElement s3table;
	
	@FindBy(how=How.NAME, using="file")
	public static WebElement browse;
	
}
	